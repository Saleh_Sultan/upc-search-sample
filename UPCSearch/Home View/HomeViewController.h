//
//  HomeViewController.h
//  UPCSearch
//
//  Created by A. K. M. Saleh Sultan on 2/19/14.
//  Copyright (c) 2014 swordfish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>

@interface HomeViewController : UIViewController < NSXMLParserDelegate >


- (IBAction)sumbitButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtUpcCode;
@property (weak, nonatomic) IBOutlet UITextView *txtTitleArea;

@end
