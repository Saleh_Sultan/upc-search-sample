//
//  HomeViewController.m
//  UPCSearch
//
//  Created by A. K. M. Saleh Sultan on 2/19/14.
//  Copyright (c) 2014 swordfish. All rights reserved.
//

#import "HomeViewController.h"
#import "GTMNSString+URLArguments.h"
#import "MBProgressHUD.h"
#import "GTMBase64.h"


#define AccessKeyId                 @"AKIAJQVURFHMEI3OGBTQ"
#define SecretAccessKey             @"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"
#define AssociateTag                @"loyajust-20"
#define CC_SHA256_DIGEST_LENGTH     32


@interface HomeViewController ()
{
    MBProgressHUD *HUD;
    BOOL isItemLookUpSearch;
    NSMutableData   *responseData;
    NSMutableArray *arrAWSItemLookUpProducts;
    NSMutableDictionary *dictXMLData;
    NSString *responseString, *strSelectedTag;
}
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Return the time stamp for AWS
- (NSString *)utcTimestamp
{
	NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
	outputFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
	outputFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    return [outputFormatter stringFromDate:[NSDate date]];
}

//Return the Signature for AWS
- (NSString *)hmacStringForString:(NSString *)signatureInput
{
	unsigned char digest[CC_SHA256_DIGEST_LENGTH] = {0};
    NSString* key = SecretAccessKey;
	char *keychar = strdup([key UTF8String]);
	char *datachar = strdup([signatureInput UTF8String]);
	CCHmacContext hctx;
	CCHmacInit(&hctx, kCCHmacAlgSHA256, keychar, strlen(keychar));
	CCHmacUpdate(&hctx, datachar, strlen(datachar));
	CCHmacFinal(&hctx, digest);
	return [GTMBase64 stringByEncodingBytes:digest length:CC_SHA256_DIGEST_LENGTH];
	
}

//Call this method to call AWS Itemlookup webservice
-(void)callAWSItemLookUPWithCodeType:(NSString*)codeType
{
    isItemLookUpSearch = TRUE;
    NSString *timeStr  = [[self utcTimestamp] gtm_stringByEscapingForURLArgument];
    
    NSLog(@"timestr %@",timeStr);
    
    //Create AWS Signature Input
    NSString *tmpStr = [NSString stringWithFormat:@"GET\nwebservices.amazon.com\n/onca/xml\nAWSAccessKeyId=%@&AssociateTag=%@&IdType=%@&ItemId=%@&Operation=ItemLookup&ResponseGroup=Large&SearchIndex=All&Service=AWSECommerceService&Timestamp=%@",AccessKeyId,AssociateTag,codeType,self.txtUpcCode.text, timeStr];
    //ResponseGroup
    
    //Create AWS Signature
    NSString *signature = [[self hmacStringForString:tmpStr] gtm_stringByEscapingForURLArgument];
    
    NSString *urlString = [NSString stringWithFormat:@"http://webservices.amazon.com/onca/xml?AWSAccessKeyId=%@&AssociateTag=%@&IdType=%@&ItemId=%@&Operation=ItemLookup&ResponseGroup=Large&SearchIndex=All&Service=AWSECommerceService&Timestamp=%@&Signature=%@",AccessKeyId,AssociateTag,codeType,self.txtUpcCode.text,timeStr,signature];
    
    NSLog(@"urlString1 = %@",urlString);
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    NSMutableURLRequest *urlRequest=[NSMutableURLRequest requestWithURL:url];
    url	= nil;
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLConnection *connectionResponse = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    if (!connectionResponse)
    {
        NSLog(@"Failed to submit request");
    }
    else
    {
        [self.txtUpcCode resignFirstResponder];
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        responseData=[[NSMutableData data] copy];
    }
    connectionResponse = nil;
    urlRequest = nil;
    
}


- (IBAction)sumbitButtonAction:(id)sender {
}



#pragma mark connection delegate
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData=nil;
    responseData =[[NSMutableData alloc] init ];
    [responseData setLength: 0];
    
    //expectedLength = [response expectedContentLength];
	//currentLength = 0;
	HUD.mode = MBProgressHUDModeDeterminate;
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
    //currentLength += [data length];
	//HUD.progress = currentLength / (float)expectedLength;
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [HUD hide:YES];
	UIAlertView *message = [[UIAlertView alloc]
							initWithTitle:@"ERROR"
							message:[NSString stringWithFormat:@"%@",[error description]]
							delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[message show];
}

//Get the returned JSON result from server and parse it.
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
    [HUD hide:YES afterDelay:1];
    
        NSLog(@"=============%@",[NSString stringWithUTF8String:[responseData bytes]]);
        
        NSXMLParser *xmlParser=nil;
        xmlParser= [[NSXMLParser alloc] initWithData:responseData];
        
        //Set delegate
        [xmlParser setDelegate:self];
        
        //Start parsing the XML file.
        [xmlParser parse];
    
}



#pragma mark XML parser Delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName
	attributes:(NSDictionary *)attributeDict
{
        
        if([elementName isEqualToString:@"ItemLookupResponse"])
        {
            arrAWSItemLookUpProducts = [[NSMutableArray alloc] init];
        }
        else {
            if ([elementName isEqualToString:@"Item"])
            {
                dictXMLData =[[NSMutableDictionary alloc] init];
                [dictXMLData setValue:self.txtUpcCode.text forKey:@"upc"];
            }
            else if ([elementName isEqualToString:@"MediumImage"])
                {
                    strSelectedTag = @"MediumImage";
                }
                else if ([elementName isEqualToString:@"ItemAttributes"])
                    {
                        strSelectedTag = @"ItemAttributes";
                    }
        }

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	responseString = [[NSString alloc] initWithString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (isItemLookUpSearch)//Parse Item LookUP
    {
        if([elementName isEqualToString:@"ItemLookupResponse"])
        {
            if ([arrAWSItemLookUpProducts count]>0)
            {
                //ProductDetail *productDetails = [[ProductDetail alloc] initWithItems:arrAWSItemLookUpProducts];
                //[self.navigationController pushViewController:productDetails animated:YES];
            }
        }
        else {
            if ([elementName isEqualToString:@"ASIN"])
            {
                [dictXMLData setValue:responseString forKey:@"ASIN"];
            }
            else if ([elementName isEqualToString:@"DetailPageURL"])
            {
                [dictXMLData setValue:responseString forKey:@"DetailPageURL"];
            }
            else
                if ([elementName isEqualToString:@"Manufacturer"])
                {
                    [dictXMLData setValue:responseString forKey:@"Manufacturer"];
                }
                else if ([elementName isEqualToString:@"Title"] && [strSelectedTag isEqualToString:@"ItemAttributes"])
                {
                    [dictXMLData setValue:responseString forKey:@"Title"];
                    strSelectedTag = @"";
                }
                else if ([elementName isEqualToString:@"ProductGroup"])
                {
                    [dictXMLData setValue:responseString forKey:@"ProductGroup"];
                }
                else
                    if ([elementName isEqualToString:@"URL"] && [strSelectedTag isEqualToString:@"MediumImage"])
                    {
                        [dictXMLData setValue:responseString forKey:@"imageurl"];
                        strSelectedTag = @"";
                    }
                    else
                        if ([elementName isEqualToString:@"FormattedPrice"])
                        {
                            [dictXMLData setValue:responseString forKey:@"price"];
                        }
                        else
                            if ([elementName isEqualToString:@"Height"]&& [strSelectedTag isEqualToString:@"ItemAttributes"])
                            {
                                [dictXMLData setValue:responseString forKey:@"Height"];
                            }
                            else
                                if ([elementName isEqualToString:@"Length"]&& [strSelectedTag isEqualToString:@"ItemAttributes"])
                                {
                                    [dictXMLData setValue:responseString forKey:@"Length"];
                                }
                                else
                                    if ([elementName isEqualToString:@"Weight"]&& [strSelectedTag isEqualToString:@"ItemAttributes"])
                                    {
                                        [dictXMLData setValue:responseString forKey:@"Weight"];
                                    }
                                    else
                                        if ([elementName isEqualToString:@"Width"]&& [strSelectedTag isEqualToString:@"ItemAttributes"])
                                        {
                                            [dictXMLData setValue:responseString forKey:@"Width"];
                                        }
                                        else
                                            if ([elementName isEqualToString:@"Item"])
                                            {
                                                [arrAWSItemLookUpProducts addObject:dictXMLData];
                                            }
                                            else
                                                if([elementName isEqualToString:@"Message"])
                                                {
                                                    
                                                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                                                      message:responseString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                    [message show];
                                                    
                                                }
        }
    }
    
}
@end
